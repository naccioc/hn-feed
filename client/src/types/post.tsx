export type Post = {
  _id: string;
  objectID: string;
  active: boolean;
  title: string;
  author: string;
  created_at: string;
  url: string;
};
