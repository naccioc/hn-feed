import React from 'react';
import { act, getByRole, render, waitFor } from '@testing-library/react';
import App from './App';
import axios, { AxiosResponse } from 'axios';
import { Post } from './types/post';

describe('App', () => {
  it('renders header', () => {
    const { getByRole, getByText } = render(<App />);

    expect(getByRole('heading')).toBeInTheDocument();
    expect(getByRole('heading')).toHaveTextContent('HN Feed');
    expect(getByText('We <3 hacker news!')).toBeInTheDocument();
  });

  it('renders the posts list', async () => {
    const response: AxiosResponse = {
      data: [{
        _id: '0',
        objectID: '0',
        active: true,
        title: 'Test Title',
        author: 'Test Author',
        created_at: '2021-02-03T23:26:35.000Z',
        url: 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      } as Post],
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest.spyOn(axios, 'get').mockImplementationOnce(() => Promise.resolve(response));

    await act(async () => {
      const { getByRole, getByText } = render(<App />);

      await waitFor(() => [
        expect(getByText(response.data[0].title)).toBeInTheDocument(),
        expect(getByText(`- ${response.data[0].author} -`)).toBeInTheDocument(),
        expect(getByRole('link')).toHaveAttribute('href', response.data[0].url),
      ]);
    });
  });
});
