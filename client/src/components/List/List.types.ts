import { MouseEventHandler } from "react";
import { Post } from "../../types/post";

export type ListProps = {
  posts: Post[];
  onClick: MouseEventHandler;
};
