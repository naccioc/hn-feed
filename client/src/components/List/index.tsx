import React from 'react';
import { ListProps } from './List.types';
import ListItem from './ListItem';
import './List.css';

function List({ posts, onClick }: ListProps) {
  return (
    <ul className="List">
      {posts.filter((post) => post.active).map((post) => (
        <ListItem key={post._id} post={post} onClick={onClick} />
      ))}
    </ul>
  );
}

export default List;
