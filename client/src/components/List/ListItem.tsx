import React from 'react';
import DeleteIcon from '../DeleteIcon/DeleteIcon';
import { formatDate } from '../../utils/date';
import { ListItemProps } from './ListItem.types';
import './ListItem.css';

function ListItem({ post, onClick }: ListItemProps) {
  return (
    <li className="ListItem">
        <a className="ListItem--anchor" href={post.url} target="_blank" rel="noreferrer">
          <span className="ListItem--title">{post.title}</span>
          <span className="ListItem--author"> - {post.author} - </span>
          <span className="ListItem--date">{formatDate(post.created_at)}</span>
          <button data-id={post._id} className="ListItem--delete" onClick={onClick}>
            <DeleteIcon className="ListItem--delete-icon" />
          </button>
        </a>
    </li>
  );
}

export default ListItem;
