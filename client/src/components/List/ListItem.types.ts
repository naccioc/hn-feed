import { MouseEventHandler } from "react";
import { Post } from "../../types/post";

export type ListItemProps = {
  post: Post;
  onClick: MouseEventHandler;
};
