import { format, isToday, isYesterday, parseISO } from 'date-fns';

export const formatDate = (date: string) => {
  const parsed_date = parseISO(date);
  let formated_date = '';

  if (isToday(parsed_date)) {
    formated_date = format(parsed_date, 'p');
  } else if (isYesterday(parsed_date)) {
    formated_date = 'Yesterday';
  } else {
    formated_date = format(parsed_date, 'MMM d');
  }

  return formated_date;
};
