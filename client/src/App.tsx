import axios from 'axios';
import React, { MouseEvent, useState, useEffect } from 'react';
import List from './components/List';
import { Post } from './types/post';
import './App.css';

function App() {
  const [posts, setPosts] = useState<Post[]>( [] );

  const fetch = async () => {
    return await axios.get('http://localhost:3001/posts');
  };

  const handleClick = async (event: MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();

    const { id } = (event.target as HTMLButtonElement).dataset;
    const { data } = await axios.post(`http://localhost:3001/posts/disable/${id}`);

    if (data.status === 'OK') {
      setPosts(prevPosts => prevPosts.map((post) => {
        return post._id === id ? { ...post, active: false } : post;
      }));
    }
  };

  useEffect(() => {
    fetch().then((result) => setPosts(result.data));
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-header--principal">HN Feed</h1>
        <div className="App-header--secondary">{'We <3 hacker news!'}</div>
      </header>
      <main className="App-main">
        <nav className="App-navigation">
          <List posts={posts} onClick={handleClick} />
        </nav>
      </main>
    </div>
  );
}

export default App;
