# HN Feed
Read the latests HN post about Node.js

## Requirements
- Node.js 14.x
- Docker

## How to run

For a production build/run:
```shell
$ docker-compose up --build
```
Then you can access the front page through http://localhost:8080, and the API through http://localhost:3001

For development:
```shell
$ docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build
```
Then you can access the front page through http://localhost:3000, and the API through http://localhost:3001

If you rather want to run the tests:
```shell
$ docker-compose -f docker-compose.yml -f docker-compose.test.yml up --build
```

