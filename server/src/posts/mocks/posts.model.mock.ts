/* eslint-disable @typescript-eslint/no-empty-function */
export default class PostsModelMock {
  constructor(public data?: any) {}

  find() {
    return this;
  }

  sort() {
    return this;
  }

  async exec(): Promise<void> {}
  async create(): Promise<void> {}
  async insertMany(): Promise<void> {}
  async updateOne(): Promise<void> {}
}
