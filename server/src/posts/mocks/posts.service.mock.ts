/* eslint-disable @typescript-eslint/no-empty-function */
export class PostsServiceMock {
  async get(): Promise<void> {}
  async disable(): Promise<void> {}
}
