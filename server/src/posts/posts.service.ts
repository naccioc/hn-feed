import { HttpService, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model, Types } from 'mongoose';
import { Post, PostDocument } from './schemas/post.schema';
import { HNPost, SyncStatus } from './types/posts.types';
import { AxiosResponse } from 'axios';

@Injectable()
export class PostsService implements OnModuleInit {
  private readonly logger = new Logger(PostsService.name);

  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    private http: HttpService,
  ) {}

  async onModuleInit(): Promise<void> {
    await this.sync();
  }

  async get(): Promise<Post[]> {
    return this.postModel
      .find({ active: true })
      .sort({ created_at: -1 })
      .exec();
  }

  async disable(id: Types.ObjectId): Promise<SyncStatus> {
    try {
      await this.postModel.updateOne({ _id: id }, { active: false });

      return {
        status: 'OK',
        message: 'The post was successfully disabled',
      };
    } catch (error) {
      return {
        status: 'error',
        message: `Couldn't disable post`,
      };
    }
  }

  @Cron(CronExpression.EVERY_HOUR)
  async sync(): Promise<void> {
    try {
      const posts = await this.fetch();

      if (posts?.data?.hits) {
        const prepared_posts = this.create(posts.data.hits);

        await this.save(prepared_posts);
      }
    } catch (error) {
      const error_message = `Couldn't sync data`;

      this.logger.error(error_message, error);
    }
  }

  async fetch(): Promise<AxiosResponse<any>> {
    return await this.http
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
  }

  async save(posts: PostDocument[]): Promise<Post[]> {
    return this.postModel.insertMany(posts);
  }

  create(posts: HNPost[]): PostDocument[] {
    return posts
      .filter((post) => post.story_url !== null || post.url !== null)
      .map((post) => {
        return {
          objectID: post.objectID,
          created_at: post.created_at,
          active: true,
          title: post.story_title || post.title,
          author: post.author,
          url: post.story_url || post.url,
        } as PostDocument;
      });
  }
}
