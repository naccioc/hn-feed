import { Test, TestingModule } from '@nestjs/testing';
import { PostsServiceMock } from './mocks/posts.service.mock';
import { PostsController } from './posts.controller';
import { PostsService } from './posts.service';
import * as PostsMock from './mocks/posts.transformed.mock.data.json';
import { Types } from 'mongoose';

describe('PostsController', () => {
  let controller: PostsController;
  let service: PostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PostsController],
      providers: [
        {
          provide: PostsService,
          useClass: PostsServiceMock,
        },
      ],
    }).compile();

    controller = module.get<PostsController>(PostsController);
    service = module.get<PostsService>(PostsService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should get the posts', async () => {
    jest
      .spyOn(service, 'get')
      .mockImplementationOnce(() => Promise.resolve(PostsMock));

    const response = await controller.get();

    expect(service.get).toHaveBeenCalled();
    expect(response).toStrictEqual(PostsMock);
  });

  it('should disable a post', async () => {
    jest.spyOn(service, 'disable');

    const id = new Types.ObjectId();

    await controller.disable(id);

    expect(service.disable).toHaveBeenCalledWith(id);
  });
});
