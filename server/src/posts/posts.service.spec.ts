import { HttpModule, HttpService } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { AxiosResponse } from 'axios';
import { Types } from 'mongoose';
import { of } from 'rxjs';
import PostsModelMock from './mocks/posts.model.mock';
import { PostsService } from './posts.service';
import { Post, PostDocument } from './schemas/post.schema';
import * as HNPostsMock from './mocks/posts.mock.data.json';
import * as PostsMock from './mocks/posts.transformed.mock.data.json';

describe('PostsService', () => {
  let service: PostsService;
  let http_service: HttpService;
  let model: PostsModelMock;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        {
          provide: getModelToken(Post.name),
          useClass: PostsModelMock,
        },
        PostsService,
      ],
    }).compile();

    service = module.get<PostsService>(PostsService);
    model = module.get<PostsModelMock>(getModelToken(Post.name));
    http_service = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should start sync post onModuleInit', async () => {
    jest.spyOn(service, 'sync').mockImplementationOnce(() => Promise.resolve());

    await service.onModuleInit();

    expect(service.sync).toHaveBeenCalled();
  });

  it('should get posts', async () => {
    jest.spyOn(model, 'find');
    jest.spyOn(model, 'sort');
    jest.spyOn(model, 'exec');

    await service.get();

    expect(model.find).toHaveBeenCalledWith({ active: true });
    expect(model.sort).toHaveBeenCalledWith({ created_at: -1 });
    expect(model.exec).toHaveBeenCalled();
  });

  it('should disable a post', async () => {
    jest
      .spyOn(model, 'updateOne')
      .mockImplementationOnce(() => Promise.resolve());

    const id = new Types.ObjectId();
    const response = await service.disable(id);

    expect(model.updateOne).toHaveBeenCalledWith(
      { _id: id },
      { active: false },
    );
    expect(response).toEqual({
      status: 'OK',
      message: 'The post was successfully disabled',
    });
  });

  it('should fail to disable a post', async () => {
    jest
      .spyOn(model, 'updateOne')
      .mockImplementationOnce(() => Promise.reject());

    const id = new Types.ObjectId();
    const response = await service.disable(id);

    expect(model.updateOne).toHaveBeenCalledWith(
      { _id: id },
      { active: false },
    );
    expect(response).toEqual({
      status: 'error',
      message: `Couldn't disable post`,
    });
  });

  it('should sync HN posts', async () => {
    const response_mock: AxiosResponse = {
      data: HNPostsMock,
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest
      .spyOn(service, 'fetch')
      .mockImplementationOnce(() => Promise.resolve(response_mock));
    jest.spyOn(service, 'create');
    jest.spyOn(service, 'save');

    await service.sync();

    expect(service.fetch).toHaveBeenCalled();
    expect(service.create).toHaveBeenCalledWith(response_mock.data.hits);
    expect(service.save).toHaveBeenCalled();
  });

  it('should fail to sync HN posts if cannot reach API service', async () => {
    jest.spyOn(service, 'fetch').mockImplementationOnce(() => Promise.reject());
    jest.spyOn(service, 'create');
    jest.spyOn(service, 'save');

    await service.sync();

    expect(service.fetch).toHaveBeenCalled();
    expect(service.create).not.toHaveBeenCalled();
    expect(service.save).not.toHaveBeenCalled();
  });

  it('should fail to sync HN posts if API response is empty', async () => {
    const response_mock: AxiosResponse = {
      data: [],
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest
      .spyOn(service, 'fetch')
      .mockImplementationOnce(() => Promise.resolve(response_mock));
    jest.spyOn(service, 'create');
    jest.spyOn(service, 'save');

    await service.sync();

    expect(service.fetch).toHaveBeenCalled();
    expect(service.create).not.toHaveBeenCalled();
    expect(service.save).not.toHaveBeenCalled();
  });

  it('should fetch data from HN API', async () => {
    const response_mock: AxiosResponse = {
      data: [],
      status: 200,
      statusText: 'OK',
      headers: {},
      config: {},
    };

    jest
      .spyOn(http_service, 'get')
      .mockImplementationOnce(() => of(response_mock));

    const response = await service.fetch();

    expect(http_service.get).toHaveBeenCalledWith(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    expect(response).toStrictEqual(response_mock);
  });

  it('should save to database', async () => {
    const { _id, ...rest } = PostsMock[0];
    jest
      .spyOn(model, 'insertMany')
      .mockImplementationOnce(() => Promise.resolve());

    await service.save([rest as PostDocument]);

    expect(model.insertMany).toHaveBeenCalledWith([rest as PostDocument]);
  });

  it('should create a PostDocument[] from HNPost[]', async () => {
    const { _id, ...rest } = PostsMock[0];
    const response = service.create([HNPostsMock.hits[0]]);

    expect(response).toStrictEqual([rest]);
  });

  it('should create a PostDocument[] from HNPost[] if story_title is null', async () => {
    const { _id, ...rest } = PostsMock[0];
    const post = HNPostsMock.hits[0];

    post.title = post.title || post.story_title;
    post.story_title = null;

    const response = service.create([post]);

    expect(response).toStrictEqual([rest]);
  });

  it('should create a PostDocument[] from HNPost[] if story_url is null', async () => {
    const { _id, ...rest } = PostsMock[0];
    const post = HNPostsMock.hits[0];

    post.url = post.url || post.story_url;
    post.story_url = null;

    const response = service.create([post]);

    expect(response).toStrictEqual([rest]);
  });
});
