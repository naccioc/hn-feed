import { Controller, Get, Param, Post } from '@nestjs/common';
import { Types } from 'mongoose';
import { PostsService } from './posts.service';

@Controller('posts')
export class PostsController {
  constructor(private readonly postsService: PostsService) {}

  @Get()
  async get() {
    return this.postsService.get();
  }

  @Post('disable/:id')
  async disable(@Param('id') id: Types.ObjectId) {
    return this.postsService.disable(id);
  }
}
