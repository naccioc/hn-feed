import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {
  @Prop({ required: true, unique: true })
  objectID: string;

  @Prop({ required: true })
  created_at: string;

  @Prop({ required: true, default: false })
  active: boolean;

  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  author: string;

  @Prop()
  url: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);
